module gitlab.com/mmmicke/streamx

go 1.16

require (
	github.com/google/uuid v1.2.0 // indirect
	github.com/labstack/echo/v4 v4.2.0
	golang.org/x/net v0.0.0-20210226172049-e18ecbb05110
)
