# streamx

This is an experiment with pushing html through websockets using Go.

Inspired by:
https://alistapart.com/article/the-future-of-web-software-is-html-over-websockets/

## Start
```bash
go run main.go
```
