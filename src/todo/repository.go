package todo

import (
	"github.com/google/uuid"
	"gitlab.com/mmmicke/streamx/src/domain"
)

type repository struct {
	list []domain.Todo
}

func NewTodoRepository() domain.TodoRepository {
	return &repository{}
}

func (r *repository) Save(todo domain.Todo) error {
	todo.ID = uuid.New().String()
	r.list = append(r.list, todo)
	return nil
}

func (r *repository) GetAll() ([]domain.Todo, error) {
	return r.list, nil
}

func (r *repository) Toggle(ID string) error {
	for index, item := range r.list {
		if item.ID == ID {
			r.list[index].Done = !item.Done
		}
	}
	return nil
}