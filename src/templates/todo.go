package templates

import (
	"errors"
	"gitlab.com/mmmicke/streamx/src/domain"
	"html/template"
	"io"
	"io/fs"
)

type TodoTemplate struct {
	todoRepo domain.TodoRepository
}

func NewTodoTemplate(todoRepo domain.TodoRepository) *TodoTemplate {
	return &TodoTemplate{
		todoRepo: todoRepo,
	}
}


type TodoPageData struct {
	PageTitle string
	Todos     []domain.Todo
}

func (t *TodoTemplate) RenderTodo(w io.Writer) error {
	tmpl, err := template.ParseFiles("src/templates/todo.html")
	if err != nil {
		if errors.Is(err, fs.ErrNotExist) {
			return err
		}
		return err
	}

	todos, err := t.todoRepo.GetAll()

	data := TodoPageData{
		PageTitle: "My TODO list",
		Todos: todos,
	}

	return tmpl.Execute(w, data)
}
