package ws

import (
	"fmt"
	"strconv"
	strings "strings"
)

type userRequest struct {
	UserID int
}

func handleUserRequest(msg string) (string, error) {
	req, err := parseRequest(msg)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("<h2>User %d is here!</h2>", req.UserID), nil
}

func parseRequest(msg string) (*userRequest, error) {
	userID, err := strconv.Atoi(strings.Split(msg, "/")[1])
	if err != nil {
		return nil, err
	}
	return &userRequest{
		UserID: userID,
	}, nil
}
