package ws

import (
	"bytes"
	"encoding/json"
	"github.com/labstack/echo/v4"
	"gitlab.com/mmmicke/streamx/src/handlers"
	"gitlab.com/mmmicke/streamx/src/transport"
	"golang.org/x/net/websocket"
)

type WebsocketHandler interface {
	Handle(ctx echo.Context) error
}

type handler struct {
	todoHandler *handlers.TodoHandler
}

func New(todoHandler *handlers.TodoHandler) WebsocketHandler {
	return &handler{
		todoHandler,
	}
}

func (h *handler) Handle(c echo.Context) error {
	websocket.Handler(func(ws *websocket.Conn) {
		defer ws.Close()
		for {
			msg := ""
			err := websocket.Message.Receive(ws, &msg)
			if err != nil {
				if err.Error() == "EOF" {
					return
					//c.Logger().Error(err)
				} else {
					c.Logger().Error(err)
				}
			} else {
				err = h.handle(c, ws, msg)
				if err != nil {
					c.Logger().Error(err)
				}
			}
		}
	}).ServeHTTP(c.Response(), c.Request())
	return nil
}

func (h *handler) handle(c echo.Context, ws *websocket.Conn, msg string) error {
	command := transport.Command{}
	err := json.Unmarshal([]byte(msg), &command)
	if err != nil {
		c.Logger().Error(err)
		return transport.ErrUnknownCommand
	}
	var buf bytes.Buffer
	err = h.todoHandler.HandleTodoCommand(&buf, command)
	if err != nil {
		return err
	}
	return respond(ws, buf.String())
}

func respond(ws *websocket.Conn, response string) error {
	return websocket.Message.Send(ws, response)
}
