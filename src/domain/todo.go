package domain

type Todo struct {
	ID    string
	Title string
	Done  bool
}

type TodoRepository interface {
	GetAll() ([]Todo, error)
	Save(todo Todo) error
	Toggle(ID string) error
}
