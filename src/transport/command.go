package transport

import "encoding/json"

type Command struct {
	Action string          `json:"action"`
	Data   json.RawMessage `json:"data"`
}
