package transport

import "errors"

var (
	ErrUnknownCommand = errors.New("Unknown command")
)
