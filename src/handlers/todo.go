package handlers

import (
	"encoding/json"
	"gitlab.com/mmmicke/streamx/src/domain"
	"gitlab.com/mmmicke/streamx/src/templates"
	"gitlab.com/mmmicke/streamx/src/transport"
	"io"
)

type TodoHandler struct {
	template *templates.TodoTemplate
	repo     domain.TodoRepository
}

func NewTodoHandler(repo domain.TodoRepository, template *templates.TodoTemplate) *TodoHandler {
	return &TodoHandler{
		template: template,
		repo:     repo,
	}
}

type AddTodoData struct {
	Title string `json:"title"`
}

type ToggleTodoData struct {
	ID string `json:"id"`
}

type ActionResponse struct {
	Action  string `json:"action"`
	Success bool   `json:"success"`
	Error   string `json:"error"`
}

type GetTodoData struct {
	ID string `json:"id"`
}

func (h *TodoHandler) HandleTodoCommand(w io.Writer, input transport.Command) error {
	if input.Action == "todo/add" {
		data := AddTodoData{}
		err := json.Unmarshal(input.Data, &data)
		if err != nil {
			return err
		}

		t := domain.Todo{
			Title: data.Title,
			Done:  false,
		}
		err = h.repo.Save(t)
		if err != nil {
			return errorResponse(w, input.Action, err)
		}
		return successResponse(w, input.Action)
	} else if input.Action == "todo/get" {
		return h.template.RenderTodo(w)
	} else if input.Action == "todo/toggle" {
		data := ToggleTodoData{}
		err := json.Unmarshal(input.Data, &data)
		if err != nil {
			return err
		}

		err = h.repo.Toggle(data.ID)
		if err != nil {
			return errorResponse(w, input.Action, err)
		}
		return successResponse(w, input.Action)
	}
	return transport.ErrUnknownCommand
}

func successResponse(w io.Writer, action string) error {
	response, err := json.Marshal(ActionResponse{
		Action:  action,
		Success: true,
	})
	if err != nil {
		return err
	}
	_, err = w.Write(response)
	return err
}

func errorResponse(w io.Writer, action string, responseError error) error {
	response, err := json.Marshal(ActionResponse{
		Action:  action,
		Success: false,
		Error:   responseError.Error(),
	})
	if err != nil {
		return err
	}
	_, err = w.Write(response)
	return err
}
