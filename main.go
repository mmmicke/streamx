package main

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/mmmicke/streamx/src/handlers"
	templates "gitlab.com/mmmicke/streamx/src/templates"
	"gitlab.com/mmmicke/streamx/src/todo"
	"gitlab.com/mmmicke/streamx/src/ws"
)

func main() {

	todoRepo := todo.NewTodoRepository()
	todoTemplate := templates.NewTodoTemplate(todoRepo)
	todoHandler := handlers.NewTodoHandler(todoRepo, todoTemplate)
	wsHandler := ws.New(todoHandler)


	e := echo.New()
	e.Static("/demo", "public")
	e.GET("/ws", wsHandler.Handle)
	//e.Static("/", "web/build")
	e.Logger.Fatal(e.Start(":8888"))
}
