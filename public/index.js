var loc = window.location;
var uri = 'ws:';
if (loc.protocol === 'https:') {
    uri = 'wss:';
}
uri += '//' + loc.host + '/ws';

viewWS = new WebSocket(uri)
actionWS = new WebSocket(uri)

viewWS.onopen = function () {
    console.log('View client connected')
    updateTodoView()
}
actionWS.onopen = function () {
    console.log('Action client connected')
}

viewWS.onmessage = function (evt) {
    console.log("Incoming view data", evt.data)
    document.getElementById('body').innerHTML = evt.data;
}
actionWS.onmessage = function (evt) {
    console.log("Command finished:", evt.data)
    const response = JSON.parse(evt.data)
    console.log("Action response", response)
    if (response.action === "todo/add" || response.action === "todo/toggle") {
        updateTodoView();
    }
}

function sendActionCommand(action, data) {
    actionWS.send(JSON.stringify({
        action,
        data
    }));
}

function updateTodoView() {
    sendViewCommand("todo/get")
}

function sendViewCommand(action) {
    viewWS.send(JSON.stringify({
        action,
    }));
}

function addTodo(event) {
    const title = document.getElementById("title").value;
    sendActionCommand("todo/add", {title});
}

function toggleTodoItem(id) {
    sendActionCommand("todo/toggle", {id});
}